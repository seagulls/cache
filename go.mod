module gitlab.com/seagulls/cache

go 1.18

require (
	github.com/frankban/quicktest v1.12.0
	github.com/gorilla/mux v1.8.0
)

require (
	github.com/google/go-cmp v0.5.5 // indirect
	github.com/kr/pretty v0.2.1 // indirect
	github.com/kr/text v0.2.0 // indirect
	golang.org/x/xerrors v0.0.0-20200804184101-5ec99f83aff1 // indirect
)
