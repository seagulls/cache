package cache_test

import (
	"testing"
	"time"

	qt "github.com/frankban/quicktest"
	"gitlab.com/seagulls/cache/lib/cache"
)

func TestCache_NormalBehaviour(t *testing.T) {
	c := qt.New(t)
	ca := cache.New[string](time.Second, 5*time.Second)

	expected := "Test Get is a String"
	_ = ca.Put("key", expected, 10*time.Second)

	actual, state := ca.Get("key")
	c.Assert(state, qt.IsTrue)
	c.Assert(actual, qt.Equals, expected)
}

func TestCache_Keys(t *testing.T) {
	c := qt.New(t)
	ca := cache.New[string](time.Second, 5*time.Second)

	store := "Test Get is a String"
	_ = ca.Put("key", store, 10*time.Second)

	expected := []string{"key"}
	actual := ca.Keys()
	c.Assert(actual, qt.DeepEquals, expected)
}

func TestCache_GetReturnsMiss(t *testing.T) {
	c := qt.New(t)
	ca := cache.New[string](time.Second, 5*time.Second)

	actual, state := ca.Get("key")
	c.Assert(state, qt.Not(qt.IsTrue))
	c.Assert(actual, qt.Equals, "")
}

func TestCache_DoublePutOverwrites(t *testing.T) {
	c := qt.New(t)
	ca := cache.New[string](time.Second, 5*time.Second)

	overwritten := "Test Get gets overwritten"
	expected := "Test Get is a String"

	_ = ca.Put("key", overwritten, 10*time.Second)
	_ = ca.Put("key", expected, 10*time.Second)

	actual, state := ca.Get("key")
	c.Assert(state, qt.IsTrue)
	c.Assert(actual, qt.Equals, expected)
}

func TestCache_Expires(t *testing.T) {
	c := qt.New(t)
	ca := cache.New[string](5*time.Millisecond, 5*time.Millisecond)

	expected := "Test Get is a String"
	_ = ca.Put("key", expected, 15*time.Millisecond)

	actual, state := ca.Get("key")
	c.Assert(state, qt.IsTrue)
	c.Assert(actual, qt.Equals, expected)

	time.Sleep(50 * time.Millisecond)

	actual, state = ca.Get("key")
	c.Assert(state, qt.Not(qt.IsTrue))
	c.Assert(actual, qt.Equals, "")
}

func TestCache_Hook(t *testing.T) {
	c := qt.New(t)
	ca := cache.New[string](5*time.Millisecond, 5*time.Millisecond)

	ca.Hook(
		func(item cache.Container[string]) bool {
			item.SetTTL(time.Second)
			return true
		},
		cache.HookGet,
	)

	shutdown := make(chan bool)
	ca.Hook(
		func(_ cache.Container[string]) bool {
			defer close(shutdown)
			shutdown <- true
			return true
		},
		cache.HookShutdown,
	)

	var expected interface{} = "Test Get is a String"
	_ = ca.Put("key", expected.(string), 15*time.Millisecond)

	// Get hook should extend the container TTL by one second
	actual, state := ca.Get("key")
	c.Assert(state, qt.IsTrue)
	c.Assert(actual, qt.Equals, expected)

	time.Sleep(50 * time.Millisecond)

	actual, state = ca.Get("key")
	c.Assert(state, qt.IsTrue)
	c.Assert(actual, qt.Equals, expected)

	ca.Shutdown()
	select {
	case v := <-shutdown:
		c.Assert(v, qt.IsTrue)
	case <-time.After(25 * time.Millisecond):
		c.FailNow()
	}
}
