package cache

import (
	"sync"
	"time"
)

type Closure[T any] func(Container[T]) bool
type Binding int

const (
	ExpireNever = 1 << iota
	Expired
)

const (
	HookPut      Binding = 1 << iota // put
	HookGet                          // get
	HookSweep                        // sweep
	HookPreSweep                     // presweep
	HookClean                        // clean
	HookPreClean                     // preclean
	HookShutdown                     // shutdown
)

type Container[T any] interface {
	Expired() bool
	Expires() time.Time
	SetTTL(duration time.Duration)
	Get() T
	Mark(byte)
	Scan() byte
	Refresh(time.Duration)
}

// Cache is a basic TTL cache
type Cache[T any] struct {
	items map[string]Container[T]
	mut   sync.RWMutex
	hooks map[Binding][]Closure[T]

	kill chan bool
}

func New[T any](sweep, clean time.Duration) *Cache[T] {
	r := &Cache[T]{
		items: make(map[string]Container[T]),
		mut:   sync.RWMutex{},
		hooks: make(map[Binding][]Closure[T]),

		kill: make(chan bool, 1),
	}

	sweepTicker := time.NewTicker(sweep)
	cleanTicker := time.NewTicker(clean)
	go func() {
		defer r.runHooks(nil, HookShutdown)

	controller:
		for {
			select {
			case <-r.kill:
				break controller
			case <-sweepTicker.C:
				r.sweep()
			case <-cleanTicker.C:
				r.clean()
			}
		}
	}()

	return r
}

// Get returns the cache Container associated with the key
func (c *Cache[T]) Get(key string) (v T, ok bool) {
	c.mut.RLock()
	defer c.mut.RUnlock()

	// if an item has expired, return nothing as it shouldn't be there!
	if item, ok := c.items[key]; ok {
		if item.Expired() {
			return v, false
		}
		c.runHooks(item, HookGet)
		return item.Get(), true
	}

	return v, false
}

// Put adds a new item to the cache under the specified key
func (c *Cache[T]) Put(key string, item T, ttl time.Duration) error {
	c.mut.Lock()
	defer c.mut.Unlock()

	cacheItem := NewItem(item, ttl)
	c.runHooks(cacheItem, HookPut)
	c.items[key] = cacheItem

	return nil
}

// Hook binds a closure to be executed at the desired point
func (c *Cache[T]) Hook(fn Closure[T], to Binding) {
	hooks, ok := c.hooks[to]
	if !ok {
		hooks = make([]Closure[T], 0, 1)
	}

	c.hooks[to] = append(hooks, fn)
}

// Keys returns an array of strings representing the chache keys stored
func (c *Cache[T]) Keys() []string {
	keys := make([]string, 0, len(c.items))

	for key := range c.items {
		keys = append(keys, key)
	}

	return keys
}

// Shutdown attempts to shut down the cache sweep & clean process
func (c *Cache[T]) Shutdown() {
	select {
	case c.kill <- true:
	}
}

// runHooks is an internal function running every hook bound to the given area
func (c *Cache[T]) runHooks(args Container[T], at Binding) {
	hooks, ok := c.hooks[at]
	if !ok {
		return
	}

	for _, fn := range hooks {
		fn(args)
	}
}

func (c *Cache[T]) sweep() {
	c.mut.RLock()
	defer c.mut.RUnlock()

	c.runHooks(nil, HookPreSweep)

	for k := range c.items {
		if (c.items[k].Scan()&ExpireNever == 0) && c.items[k].Expires().Before(time.Now()) {
			c.runHooks(c.items[k], HookSweep)
			c.items[k].Mark(Expired)
		}
	}
}

func (c *Cache[T]) clean() {
	c.mut.Lock()
	defer c.mut.Unlock()

	c.runHooks(nil, HookPreClean)

	for k := range c.items {
		if c.items[k].Expired() {
			c.runHooks(c.items[k], HookClean)
			delete(c.items, k)
		}
	}
}
