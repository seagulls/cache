package cache

import (
	"sync"
	"time"
)

type Item[T any] struct {
	item    T
	expires time.Time
	flags   byte
	mut     *sync.RWMutex
}

func NewItem[T any](item T, ttl time.Duration) *Item[T] {
	return &Item[T]{
		item:    item,
		expires: time.Now().Add(ttl),
		mut:     &sync.RWMutex{},
	}
}

func (i *Item[T]) Get() T {
	i.mut.RLock()
	defer i.mut.RUnlock()
	return i.item
}

func (i *Item[T]) Refresh(ttl time.Duration) {
	i.mut.Lock()
	defer i.mut.Unlock()
	i.expires = time.Now().Add(ttl)
}

func (i *Item[T]) Mark(flag byte) {
	i.mut.Lock()
	defer i.mut.Unlock()
	i.flags |= flag
}

func (i *Item[T]) Scan() byte {
	i.mut.RLock()
	defer i.mut.RUnlock()
	return i.flags
}

func (i *Item[T]) Expired() bool {
	i.mut.RLock()
	defer i.mut.RUnlock()
	return (i.flags & Expired) > 0
}

func (i *Item[T]) Expires() time.Time {
	i.mut.RLock()
	defer i.mut.RUnlock()
	return i.expires
}

func (i *Item[T]) SetTTL(ttl time.Duration) {
	i.mut.Lock()
	defer i.mut.Unlock()
	i.expires = time.Now().Add(ttl)
}
