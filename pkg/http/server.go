package http

import (
	"io/ioutil"
	"log"
	"net/http"
	"strconv"
	"time"

	"github.com/gorilla/mux"
	"gitlab.com/seagulls/cache/lib/cache"
)

// Store represents a cache store
type Store[T any] interface {
	Get(key string) (T, bool)
	Put(key string, item T, ttl time.Duration) error
	Shutdown()
}

type Server struct {
	c Store[string]
	r *mux.Router
}

func New() *Server {
	s := &Server{
		c: cache.New[string](1*time.Second, 5*time.Second),
	}

	return s
}

func (s *Server) Start() error {
	defer s.c.Shutdown()

	s.r = mux.NewRouter()

	s.r.NotFoundHandler = http.HandlerFunc(func(writer http.ResponseWriter, request *http.Request) {
		writer.WriteHeader(http.StatusMethodNotAllowed)
		_, _ = writer.Write([]byte("{\"error\":\"Method Not Allowed\"}"))
	})
	s.r.HandleFunc("/{key}", s.Get).Methods("GET")
	s.r.HandleFunc("/{key}/{generic}", s.Post).Methods("POST")

	// Start the http http and pass any terminal error through to the logger
	srv := &http.Server{
		Handler: s.r,
		Addr:    ":1234",
	}

	if err := srv.ListenAndServe(); err != nil {
		log.Printf("listen and http error: %v", err)
	}

	return nil
}

func (s *Server) Get(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	x, found := s.c.Get(vars["key"])
	w.Header().Set("Content-Type", "application/json")
	if !found {
		w.WriteHeader(http.StatusNotFound)
		_, _ = w.Write([]byte(`{"error":"KEY_NOT_FOUND"}`))
		return
	}
	_, _ = w.Write([]byte(x))
}

func (s *Server) Post(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	bytestream, err := ioutil.ReadAll(r.Body)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	t, err := strconv.Atoi(vars["generic"])
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	err = s.c.Put(vars["key"], string(bytestream), time.Second*time.Duration(t))
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	w.WriteHeader(http.StatusOK)
}
