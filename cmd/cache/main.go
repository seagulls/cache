package main

import (
	"log"

	"gitlab.com/seagulls/cache/pkg/http"
)

func main() {
	s := http.New()

	err := s.Start()
	if err != nil {
		log.Fatal(err)
	}
}
